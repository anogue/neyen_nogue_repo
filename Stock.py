#-----------------------------------
#------------- PART 2 --------------
#-----------------------------------

import matplotlib.pyplot as plt
import plotly as py
import plotly.graph_objs as go
import os
import datetime
import pandas_datareader.data as web
import numpy as np
import random
#-----

### Preliminary work
start = datetime.datetime(2005, 1, 1) # defining dates for the plot
end = datetime.datetime(2016, 12, 31)
list = [] # the list which will contain the data
list_stock = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM'] # just for obtaining the list of data
for i in list_stock:
    list.append(web.DataReader(i, 'yahoo', start, end))

### Defining class Stock
class Stock(object):
    def __init__(self, startdate, enddate, number):
        self.startdate = startdate
        self.enddate = enddate
        self.number = number # number is the position on list (will therefore give out a Stock)
        self.data = list[number].truncate(startdate, enddate) # choosed Stock

    def stock_return(self, date): # code for computing the return ob buying a stock at a given date
        x = self.data.index.searchsorted(date)
        begin = self.buy_price()
        end = self.data.iloc[x, 3]
        ret = (end - begin) / begin
        return ret

    def buy_price(self): # code for obtaining the price at which the stock has been bought
        return self.data.iloc[0, 3]

    def given_price(self, date): # code for obtaining the price of a stock at a given date
        x = self.data.index.searchsorted(date)
        price = self.data.iloc[x,3]
        return price

##### Plots

x1 = list[0]['Close'].plot(label='FDX')
x2 = list[1]['Close'].plot(label='GOOGL')
x3 = list[2]['Close'].plot(label='XOM')
x4 = list[3]['Close'].plot(label='KO')
x5 = list[4]['Close'].plot(label='NOK')
x6 = list[5]['Close'].plot(label='MS')
x7 = list[6]['Close'].plot(label='IBM')
x1.legend(loc='center left', bbox_to_anchor=(0., 0.75))
plt.grid(True)
plt.title('Stocks Prices from 01/01/2005 to 31/12/2016')
plt.xlabel('Period')
plt.ylabel('Price')
plt.show()
