#-----------------------------------
#------------- PART 4 --------------
#-----------------------------------

from Stock import Stock
from Stock import list
from Stock import list_stock
from Bond import LongTermBond
from Bond import ShortTermBond
from Bond import Bond
import random
import datetime
import pandas as pd
import numpy as np
from Investors import modelling_defensive
from Investors import defensive

class investor2(object):
    def __init__(self,budget):
        self.budget = budget

long = LongTermBond(1,1) # arbitrary bonds used for calling classes LongTermBond and ShortTermBond
short = ShortTermBond(1,1)
bond_type1 = []  # for DF
bond_type2 = []  # for DF


class mixed2(investor2):
    def __init__(self, budget):
        self.budget = budget
        self.portfolio = []
        self.type = []              # for DF
        self.product = []           # for DF
        self.spent = []             # for DF
        self.remaining_budget = []  # for DF
# Now the loop while is before if, in ths case, as long as the budget is larger than short.min_amount, the mixed investor will invest with 50-50 in bonds or stocks
        while self.budget >= short.min_amount:
            choice = random.randint(1,2)
            if choice == 1: # defensive code
                if self.budget < long.min_amount:
                    amount = random.randint(short.min_amount, int(self.budget))
                    purchase = ShortTermBond(short.term, amount)
                    self.portfolio.append(purchase)
                    self.budget -= amount
                    bond_type2.append("Short Term")  # for DF
                else:
                    if random.choice(["LT", "ST"]) == "LT":
                        # in this case the investor will buy a Long Term Bond
                        amount = random.randint(long.min_amount, int(self.budget))  # random quantity to be purchased
                        purchase = LongTermBond(long.term, amount)  # the purchased bond, 50 years maturity, cost of the random amount
                        self.portfolio.append(purchase)  # add the purchase in the list of purchases
                        self.budget -= amount
                        bond_type2.append("Long Term")  # for DF
                    else:
                        amount = random.randint(short.min_amount, int(self.budget))
                        purchase = ShortTermBond(short.term, amount)
                        self.portfolio.append(purchase)
                        self.budget -= amount
                        bond_type2.append("Short Term")  # for DF
            # code for data frame
                self.type.append(bond_type2[-1]) # if not working, try type(self.portfolio[-1])
                self.product.append("Bond")
                self.remaining_budget.append(self.budget)
                self.spent.append(amount)

            if choice == 2: # aggressive code
                start = datetime.datetime(random.randint(2005,2016),random.randint(1,12),random.randint(1,28))
                end = datetime.datetime(random.randint(start.year, 2016), random.randint(1, 12), random.randint(1, 28))
                if end < start:
                    while end < start:
                        end = datetime.datetime(random.randint(start.year, 2016), random.randint(1, 12), random.randint(1, 28))
                share_position = random.randint(0,list.__len__()-1)
                share = Stock(start, end, share_position)
                amount = random.randint(1,int(self.budget/share.given_price(start)))
                self.portfolio.append(share)
                self.budget -= amount*share.given_price(start)

                # the next four lines are for Data Frame
                self.type.append(list_stock[share_position])  # if not working, try type(self.portfolio[-1])
                self.product.append("Stock")
                self.remaining_budget.append(self.budget)
                self.spent.append(amount*share.given_price(start))

    def portfolio_history(self):
        dataframe = pd.DataFrame({'Bond or Stock?':self.product,
                                  'Type':self.type,
                                  'Cost':self.spent,
                                  'Remaining budget':self.remaining_budget})
        print(dataframe)

    def description(self):
        print("Number of shares: %s, Remaining budget: %s" % (len(self.portfolio), self.budget))

Miyazaki = mixed2(15000)
print("Miyazaki")
print(Miyazaki.portfolio_history())

### Code for modelling when starting budget is ten times higher
modelling_defensive_3 = []
number_3 = []
starting_budget_3 = 150000
while len(modelling_defensive_3)<1000:
    guy1 = defensive(starting_budget_3)
    modelling_defensive_3.append(guy1)
    number_3.append(guy1.budget)

print(pd.DataFrame({'Budget left Defensive Investor':number_3}))

### This code is for the part: In case you want more fun. Redistributing the starting budget randomly
starting_budget_2 = np.random.normal(20000,5000,1000)
modelling_defensive_2 = []
number_2 = []
while len(modelling_defensive_2)<1000:
    guy1 = defensive(starting_budget_2[len(modelling_defensive_2)])
    modelling_defensive_2.append(guy1)
    number_2.append(guy1.budget)

print(pd.DataFrame({'Budget left Defensive Investor':number_2}))
