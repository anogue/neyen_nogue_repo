#-----------------------------------
#------------- PART 3 --------------
#-----------------------------------

from Stock import Stock
from Stock import list
from Stock import list_stock
from Bond import LongTermBond
from Bond import ShortTermBond
from Bond import Bond
import random
import datetime
import pandas as pd
import numpy as np

class investor(object):
    def __init__(self,budget):
        self.budget = budget

long = LongTermBond(1,1) # arbitrary bonds used for calling classes LongTermBond and ShortTermBond
short = ShortTermBond(1,1)
bond_type1 = []  # for DF
bond_type2 = []  # for DF

class defensive(investor):
    def __init__(self, budget):
        self.budget = budget
        self.portfolio = []
        self.type = []              # for DF
        self.product = []           # for DF
        self.spent = []             # for DF
        self.remaining_budget = []  # for DF

        while self.budget >= short.min_amount:
            if self.budget < long.min_amount:
                amount = random.randint(short.min_amount, int(self.budget))
                purchase = ShortTermBond(short.term, amount)
                self.portfolio.append(purchase)
                self.budget -= amount
                bond_type1.append("Short Term") # for DF
            else:
                if random.choice(["LT", "ST"]) == "LT":
                    # in this case the investor will buy a Long Term Bond
                    amount = random.randint(long.min_amount, int(self.budget))  # random quantity to be purchased
                    purchase = LongTermBond(long.term, amount)  # the purchased bond, 50 years maturity, cost of the random amount
                    self.portfolio.append(purchase)  # add the purchase in the list of purchases
                    self.budget -= amount
                    bond_type1.append("Long Term") # for DF
                else:
                    amount = random.randint(short.min_amount, int(self.budget))
                    purchase = ShortTermBond(short.term, amount)
                    self.portfolio.append(purchase)
                    self.budget -= amount
                    bond_type1.append("Short Term") # for DF
            # code for data frame
            self.type.append(bond_type1[-1]) # if not working, try type(self.portfolio[-1])
            self.product.append("Bond")
            self.remaining_budget.append(self.budget)
            self.spent.append(amount)
        #index_df = [i for i in range(len(self.portfolio))]

    def portfolio_history(self):
        dataframe = pd.DataFrame({'Bond or Stock?':self.product,
                                  'Type of Bond': self.type,
                                  'Cost of the bond':self.spent,
                                  'Remaining budget':self.remaining_budget})
        print(dataframe)

    def description(self):
        print("Number of bonds: %s, Remaining budget: %s" %(len(self.portfolio),self.budget))
        for i in range(0,len(self.portfolio)):
            if type(self.portfolio[i])==LongTermBond:
                print("Type : Long Term Bonds Price : %s"%(self.portfolio[i].amount))
            else:
                print("Type : Short Term Bonds Price : %s"%(self.portfolio[i].amount))

### Code for checking results
Thomas = defensive(15000)
print(Thomas.description())
print(Thomas.portfolio_history())

class aggressive(investor):
    def __init__(self, budget):
        self.budget = budget
        self.portfolio = []
        self.type = []              # for DF
        self.product = []           # for DF
        self.spent = []             # for DF
        self.remaining_budget = []  # for DF
        while self.budget >= 100:
            start = datetime.datetime(random.randint(2005,2016),random.randint(1,12),random.randint(1,28))
            end = datetime.datetime(random.randint(start.year, 2016), random.randint(1, 12), random.randint(1, 28))
            if end < start:
                while end < start:
                    end = datetime.datetime(random.randint(start.year, 2016), random.randint(1, 12), random.randint(1, 28))
            share_position = random.randint(0,list.__len__()-1)
            share = Stock(start, end, share_position)
            amount = random.randint(1,int(self.budget/share.given_price(start)))
            self.portfolio.append(share)
            self.budget -= amount*share.given_price(start)

            # the next four lines are for Data Frame
            self.type.append(list_stock[share_position])  # if not working, try type(self.portfolio[-1])
            self.product.append("Stock")
            self.remaining_budget.append(self.budget)
            self.spent.append(amount*share.given_price(start))

    def portfolio_history(self):
        dataframe = pd.DataFrame({'Bond or Stock?':self.product,
                                  'Type of Stock':self.type,
                                  'Cost of the stock':self.spent,
                                  'Remaining budget':self.remaining_budget})
        print(dataframe)

    def description(self):
        print("Number of shares: %s, Remaining budget: %s" % (len(self.portfolio), self.budget))

### Code for checking results
Fanchon = aggressive(15000)
print(Fanchon.description())
print(Fanchon.portfolio_history())


class mixed(investor):
    def __init__(self, budget):
        self.budget = budget
        self.portfolio = []
        self.type = []              # for DF
        self.product = []           # for DF
        self.spent = []             # for DF
        self.remaining_budget = []  # for DF
        choice = random.randint(1,2)
        if choice == 1: # defensive code
            while self.budget >= short.min_amount:
                if self.budget < long.min_amount:
                    amount = random.randint(short.min_amount, int(self.budget))
                    purchase = ShortTermBond(short.term, amount)
                    self.portfolio.append(purchase)
                    self.budget -= amount
                    bond_type2.append("Short Term")  # for DF
                else:
                    if random.choice(["LT", "ST"]) == "LT":
                        # in this case the investor will buy a Long Term Bond
                        amount = random.randint(long.min_amount, int(self.budget))  # random quantity to be purchased
                        purchase = LongTermBond(long.term, amount)  # the purchased bond, 50 years maturity, cost of the random amount
                        self.portfolio.append(purchase)  # add the purchase in the list of purchases
                        self.budget -= amount
                        bond_type2.append("Long Term")  # for DF
                    else:
                        amount = random.randint(short.min_amount, int(self.budget))
                        purchase = ShortTermBond(short.term, amount)
                        self.portfolio.append(purchase)
                        self.budget -= amount
                        bond_type2.append("Short Term")  # for DF
                # code for data frame
                self.type.append(bond_type2[-1]) # if not working, try type(self.portfolio[-1])
                self.product.append("Bond")
                self.remaining_budget.append(self.budget)
                self.spent.append(amount)
            #if choice == 2: # aggressive code
        else:
            while self.budget >= short.min_amount:
                start = datetime.datetime(random.randint(2005,2016),random.randint(1,12),random.randint(1,28))
                end = datetime.datetime(random.randint(start.year, 2016), random.randint(1, 12), random.randint(1, 28))
                if end < start:
                    while end < start:
                        end = datetime.datetime(random.randint(start.year, 2016), random.randint(1, 12), random.randint(1, 28))
                share_position = random.randint(0,list.__len__()-1)
                share = Stock(start, end, share_position)
                amount = random.randint(1,int(self.budget/share.given_price(start)))
                self.portfolio.append(share)
                self.budget -= amount*share.given_price(start)

                # the next four lines are for Data Frame
                self.type.append(list_stock[share_position])  # if not working, try type(self.portfolio[-1])
                self.product.append("Stock")
                self.remaining_budget.append(self.budget)
                self.spent.append(amount*share.given_price(start))

    def portfolio_history(self):
        dataframe = pd.DataFrame({'Bond or Stock?':self.product,
                                  'Type':self.type,
                                  'Cost':self.spent,
                                  'Remaining budget':self.remaining_budget})
        print(dataframe)

    def description(self):
        print("Number of shares: %s, Remaining budget: %s" % (len(self.portfolio), self.budget))

### Code for checking results
Mathilde = mixed(15000)
print(Mathilde.description())
print(Mathilde.portfolio_history())

##### Modelling

modelling_defensive = []
modelling_aggressive = []
modelling_mixed = []
starting_budget = 15000

number = []
number2 = []
number3 = []

while len(modelling_defensive)<1000:
    guy1 = defensive(starting_budget)
    modelling_defensive.append(guy1)
    number.append(guy1.budget)

#### The following code does not work since there's a problem on the aggressive class

#while len(modelling_aggressive)<1000:
#    guy2 = aggressive(starting_budget)
#    modelling_aggressive.append(guy2)
#    number2.append(guy2.budget)

#while len(modelling_mixed)<1000:
#     guy3 = mixed(starting_budget)
#     modelling_mixed.append(guy3)
#     number3.append(guy3.budget)

print(pd.DataFrame({'Budget left Defensive Investor':number}))
#print(pd.DataFrame({'Budget left Aggressive Investor': number2}))
#print(pd.DataFrame({''Budget left Mixed Investor': number3}))
#print(pd.DataFrame({'Budget left Defensive Investor':number,
#                    'Budget left Aggressive Investor': number2,
#                    'Budget left Mixed Investor': number3}))
