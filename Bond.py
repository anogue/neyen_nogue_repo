#-----------------------------------
#------------- PART 1 --------------
#-----------------------------------

### First step : Define Class Bond
class Bond(object):
    def __init__(self, min_term, min_amount, term, interest):
        self.min_term = min_term
        self.min_amount = min_amount
        self.term = term
        self.interest = interest

### Second Step : Define Subclasses shortterm and longterm of the class Bond
class ShortTermBond(Bond):
    def __init__(self, inv_time, amount):
        Bond.__init__(self, min_term=3, min_amount=500, interest=0.01, term=50)
        self.inv_time = inv_time
        self.amount = amount

    def inv_return(self, inv_years):
        return ((1 + self.interest) ** (inv_years))

class LongTermBond(Bond):
    def __init__(self, inv_time, amount):
        Bond.__init__(self, min_term=5, min_amount=1000, interest=0.03, term=50)
        self.inv_time = inv_time
        self.amount = amount

    def inv_return(self, inv_years):
        return ((1 + self.interest) ** (inv_years))

### Third step : create lists containing returns of bonds
ReturnLT = []
ReturnST = []
for i in range(50):
    Short = ShortTermBond(i, 500)
    Long = LongTermBond(i,1000)
    ReturnLT.append(Long.inv_return(i))
    ReturnST.append(Short.inv_return(i))

### Fourth step : plot the lists
import matplotlib.pyplot as plt
lt, = plt.plot(ReturnLT)
st, = plt.plot(ReturnST)
plt.legend([st, lt], ['ST Bonds', 'LT Bonds'])
plt.title('Return over time (50 years) for Short and Long-Term Bonds')
plt.ylabel('Return')
plt.xlabel('Years')
plt.show()

# ### The following code aims at plotting the same graph on 75 years. Just for checking things

# ### create lists containing returns of bonds
# ReturnLT = []
# ReturnST = []
# for i in range(75):
#     Short = ShortTermBond(i, 500)
#     Long = LongTermBond(i,1000)
#     ReturnLT.append(Long.inv_return(i))
#     ReturnST.append(Short.inv_return(i))
#
# ### plot the lists
# import matplotlib.pyplot as plt
# lt, = plt.plot(ReturnLT)
# st, = plt.plot(ReturnST)
# plt.legend([st, lt], ['ST Bonds', 'LT Bonds'])
# plt.title('Return over time (75 years) for Short and Long-Term Bonds')
# plt.ylabel('Return')
# plt.xlabel('Years')
# plt.show()